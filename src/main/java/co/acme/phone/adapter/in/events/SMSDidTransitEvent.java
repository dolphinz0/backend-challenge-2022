package co.acme.phone.adapter.in.events;

import lombok.Data;

@Data
public class SMSDidTransitEvent {
    private String originatorId;
    private String calledPartyId;

    /**
     * in milliseconds since 01-Jan-1970
     */
    private Long sentDate;
}
