package co.acme.phone.adapter.in.events;

import lombok.Data;

@Data
public class CallDidTerminateEvent {

    private String originatorId;
    private String calledPartyId;

    /**
     * in milliseconds since 01-Jan-1970
     */
    private Long startDate;
    /**
     * duration of the call in milliseconds
     */
    private Long duration;

}
