package co.acme.phone.adapter.out.rest;

import co.acme.phone.domain.service.CustomerBalanceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;

/**
 * Debug endpoint for fetching a current user's balance.
 * Feel free to implement others if you find them useful, but they are not required at all.
 */
@RestController
public class CustomerBalanceResource {

    private final CustomerBalanceService customerBalanceService;

    public CustomerBalanceResource(CustomerBalanceService customerBalanceService) {
        this.customerBalanceService = customerBalanceService;
    }

    @GetMapping("/customer/{customerId}/balance")
    String getCustomerBalance(@PathVariable @NotEmpty String customerId) {
        final Long amount = customerBalanceService.amountDue(customerId);
        return formatAmount(amount);
    }

    static String formatAmount(Long amount) {
        return String.format("%.2f", ((double) amount) / 1000.0);
    }

}
