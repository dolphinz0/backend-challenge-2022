package co.acme.phone.adapter.out.events;

import lombok.Data;

@Data
public class BillItemPublishedEvent {

    private String customerId;

    /**
     * amount in thousandths of dollar, or 0.1 cent
     */
    private Long amount;

    /**
     * description appearing on the bill when the user requests it
     */
    private String description;

    /**
     * date of the charge, in milliseconds since 1970
     * we could have used {@link java.time.Instant} but that would have required more configuration
     * of the {@link com.fasterxml.jackson.databind.ObjectMapper}!
     */
    private Long date;

}
