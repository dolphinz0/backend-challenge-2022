package co.acme.phone.domain;

/**
 * Represents the type of phone call that was used
 */
public enum CallType {
    local,
    long_distance,
    international
}
