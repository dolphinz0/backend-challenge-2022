package co.acme.phone.domain.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

/**
 * Allows you to determine if the phone number is a customer
 */
@Validated
public interface CustomerService {
    boolean isCustomer(@NotEmpty String phoneNumber);
}
