package co.acme.phone.domain.service;

import org.springframework.stereotype.Component;

/**
 * returns true for any customer.
 * Let's hope users don't send angry letters because we billed them and they never heard of our company…
 */
@Component
class EveryoneIsACustomerService implements CustomerService {
    @Override
    public boolean isCustomer(String phoneNumber) {
        return true;
    }
}
