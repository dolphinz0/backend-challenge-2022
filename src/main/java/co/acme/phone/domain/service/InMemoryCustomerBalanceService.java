package co.acme.phone.domain.service;

import co.acme.phone.domain.BillItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * an in-memory implementation of the running total.
 * One could replace the hashmap with a Redis or Relational implementation, in order to have persistent storage.
 * <p>
 * But even money is fleeting… Carpe diem.
 */
@Component
class InMemoryCustomerBalanceService implements CustomerBalanceService {
    private final Logger log = LoggerFactory.getLogger(InMemoryCustomerBalanceService.class);

    private final CustomerService customerService;
    private final HashMap<String, Long> runningTotals;

    public InMemoryCustomerBalanceService(CustomerService customerService) {
        this.customerService = customerService;
        this.runningTotals = new HashMap<>();
    }

    @Override
    public void consumeBillItem(BillItem billItem) {
        final String customerId = billItem.getCustomerId();
        if (customerService.isCustomer(customerId)) {
            final long currentBalance = amountDue(customerId);
            final long billedAmount = billItem.getAmount();
            final long newBalance = currentBalance + billedAmount;
            runningTotals.put(customerId, newBalance);
            log.info("Running total for customer {} changed from {} to {}", customerId, currentBalance, newBalance);
        } else {
            throw new IllegalStateException("Unexpectedly received a bill item for a user that is not a customer! " + customerId);
        }
    }

    @Override
    public Long amountDue(String customerId) {
        return runningTotals.getOrDefault(customerId, 0L);
    }

    @Override
    public void receivePayment(String customerId, Long amount) {
        long newBalance = amountDue(customerId) - amount;
        this.runningTotals.put(customerId, newBalance);
        log.info("After a payment of {}, customer {}'s balance is {}", amount, customerId, newBalance);
    }
}
