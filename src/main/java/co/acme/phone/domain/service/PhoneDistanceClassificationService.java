package co.acme.phone.domain.service;

import co.acme.phone.domain.CallType;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

/**
 * given two phone numbers, determines whether calls between them are local, long distance, or international calls.
 */
@Validated
public interface PhoneDistanceClassificationService {
    CallType classify(@NotEmpty String originator, @NotEmpty String receivingParty);
}
