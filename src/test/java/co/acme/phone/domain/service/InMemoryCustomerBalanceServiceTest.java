package co.acme.phone.domain.service;

import co.acme.phone.domain.BillItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryCustomerBalanceServiceTest {
    private final CustomerService customerService = new EveryoneIsACustomerService();
    private InMemoryCustomerBalanceService customerBalanceService;

    private static BillItem makeBillItem(String customer, Long amount) {
        BillItem billItem = new BillItem();
        billItem.setCustomerId(customer);
        billItem.setAmount(amount);
        billItem.setInstant(Instant.now());
        return billItem;
    }

    @BeforeEach
    void resetCustomerBalanceService() {
        customerBalanceService = new InMemoryCustomerBalanceService(customerService);
    }

    @Test
    void consume2BillsForSameCustomer() {
        String customer1 = "+11283974371";

        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(0L);

        customerBalanceService.consumeBillItem(makeBillItem(customer1, 2000L));
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(2000L);

        customerBalanceService.consumeBillItem(makeBillItem(customer1, 250L));
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(2250L);
    }

    @Test
    void consume2BillsForDifferentCustomers() {
        String customer1 = "+11283974371";
        String customer2 = "+17189239590";

        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(0L);
        assertThat(customerBalanceService.amountDue(customer2)).isEqualTo(0L);

        customerBalanceService.consumeBillItem(makeBillItem(customer1, 2000L));
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(2000L);

        customerBalanceService.consumeBillItem(makeBillItem(customer2, 250L));
        assertThat(customerBalanceService.amountDue(customer2)).isEqualTo(250L);
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(2000L);
    }

    @Test
    void receivePayment() {
        String customer1 = "+198773210984";
        customerBalanceService.consumeBillItem(makeBillItem(customer1, 2000L));
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(2000L);

        customerBalanceService.receivePayment(customer1, 1000L);
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(1000L);

        customerBalanceService.receivePayment(customer1, 1000L);
        assertThat(customerBalanceService.amountDue(customer1)).isEqualTo(0L);

    }
}