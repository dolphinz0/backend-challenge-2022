package co.acme.phone.adapter.out.rest;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CustomerBalanceResourceTest {
    @Test
    void formatAmount() {
        assertThat(CustomerBalanceResource.formatAmount(1000L))
                .isEqualTo("1.00");
        assertThat(CustomerBalanceResource.formatAmount(0L))
                .isEqualTo("0.00");
    }
}