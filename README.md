# MY01 Backend Challenge

Welcome to the Backend technical challenge!

This repository contains a single README file. Start by forking this repo to a **private** personal repository. You can
use Gitlab, Github, or any other collaborative version control tool.

## The challenge!

Build a Telecom operator! (almost…)
Build a Spring application that implements a few of the following features for managing a phone company's pricing.

### About events

In an Event-Driven architecture, *eventing* refers to the use of typically short messages that convey a change in state,
known as an *event*.

Producers emit those messages, and consumers subscribe to them, so that they can take actions once they are published.

For instance, at a phone company, the *LongDistanceCallDidHangUp* event type may be used to indicate that a customer has
hung up following a long distance call. Subscribed consumers can then act on that event to calculate the new running
balance of the customer's bill.

Apache Kafka is an open-source message broker developed at LinkedIn.

Spring has developed the adapters you need to get started with event-driven services.

## Software Requirements

Software requirements are as follows:

### Functional Requirements

1. The service shall process the following domain events, with the following fields:

    - **Call Did Terminate** (Kafka topic: `co.acme.phone.customer-did-hang-up-phone-call`)
        - **Call Originator**: String (phone number)
          *the person who dialed their phone*
        - **Called Party**: String (phone number)
          *the person who received the phone call*
        - **Start Date**: Long Integer (in milliseconds since the Unix Epoch, UTC)
          *when the cell phone tower received the initial GSM connect acknowledgment*
        - **Call Duration** Long Integer (a duration in milliseconds)
          *from pick up to hang up*

    - **SMS Did Transit** (Kafka topic: `co.acme.phone.sms-did-transit`)
        - **Call Originator**: String (phone number)
          *the person who sent the SMS*
        - **Called Party**: String (phone number)
          *the person who received the SMS*
        - **Start Date**:  (in milliseconds since the Unix Epoch, UTC)
          *when the SMS was sent*

    - **Customer Did Use Data** (Kafka topic: `co.acme.phone.customer-did-use-data`)
        - **Customer ID**: String (phone number)
        - **Exempt**: Boolean (whether this data usage is free)
        - **Usage**: Long Integer (in bytes)

**Note**: The customer ID is the customer's phone number (e.g `+15556667890`, with no spaces).

2. The service shall define the following configuration properties (and the default value):
    - Price per MB (15¢)
    - Price per minute, for
        - local call (free)
        - long-distance call (8¢)
        - international call ($2.49)
    - Price per
        - outbound SMS (8¢)
        - inbound SMS (free)

3. The service shall implement, for each listed billable item, taking into account the configurable prices, a pricing
   service that posts bill items (charges) to a Kafka topic.
    1. See `co.acme.phone.adapter.out.events.BillItemPublishedEvent` for the event definition.
    2. Outbound topic for the event is `co.acme.phone.bill-item-was-published`.
    3. Extra marks: the partition key for messages should be the _customer id_. In other words, messages produced to
       this topic with the same _customer id_ will land in the same partition.

4. Consume messages from the same topic (`co.acme.phone.bill-item-was-published`) and wire them up to the provided
   bean `co.acme.phone.domain.service.CustomerBalanceService`.
   _Note_: this means that this service both Produces to the topic, as well as reads from it.

### Non-Functional Requirements

1. Keep your code organised!
    1. For example, ensure that your Kafka listeners and emitters (consumer/producer) are located
       in `co.acme.phone.adapter.in` and `co.acme.phone.adapter.out`.
    2. Similarly, keep your domain logic in `co.acme.phone.domain`. Coordinating logic may need its own package, etc.
2. Keep your commits tidy, as if you were collaborating with somebody.
3. Test your code intelligently. Keeping your code organised (it is always better to keep your classes DRY...!)

## Advice

- *Carefully review the provided code.* Several classes were provided, and they should be used to complete the
  requirements.
- *If necessary, ask questions!* It is more than possible that I have made an error in the provided code.
- *Figure out how to use a debug tool like to monitor or produce to Kafka.*
    - For example kcat (see links below) allows you to connect to a Kafka broker and monitor a topic and produce to it.
    - There also GUIs you may wish to use instead.
    - Or perhaps you may wish to use a Python script or Jupyter notebook.
- *Log your work and/or document assumptions*. Feel free to create a text file to document design decisions you made in
  creating the project.
- *Reach out with questions.* Reach out to me on LinkedIn or `alexandre[dot]cassagne[at]nxtsens[dot]com` to request
  clarifications. I will do my best to get back to you the same day or the next business day at the latest. If your
  question is of public interest, I may *update* the repo's README.md file.
- *Report issues with the base repo.* If there are issues with this repository, then create an Issue.
    - Note: issues are public.
- *Use Docker-Compose to deploy your Kafka broker*. See the provided `docker-compose.yml` which listens on port
  **29092** (feel free to modify it).

### Bonus 1

Write configuration to build and publish Docker images (hint: https://www.baeldung.com/jib-dockerizing).

### Bonus 2

Configure a CI pipeline that builds your app and runs associated tests (Gitlab, Github, Travis…).

## General Guidelines

- **Only submit your own work.**
- You are allowed to use any framework/library. But keep in mind that we might ask you to justify their use during the
  follow-up interview.
- You *must* use Spring, and best Spring practices.
- You must use Kafka as your message broker.
- You *may* create additional endpoints for testing, etc.
- Feel free to ask questions, email us or contact us on LinkedIn.

## Submission

Once satisfied with your work, invite both of us

- Eric Schaal: [Github](https://github.com/ericschaal) or [Gitlab](https://gitlab.com/ericschaal)
- Alexandre Cassange [Github](https://github.com/AlexandreCassagne) or [Gitlab](https://gitlab.com/AlexandreCassagne)

as collaborators (read-only is good enough) to your repository. Then email us or contact us on LinkedIn.

## Useful Websites (Maybe)

- [kcat](https://github.com/edenhill/kcat): Simple producer/consumer, useful in testing Kafka applications.
- [Spring Kafka Reference](https://docs.spring.io/spring-kafka/reference/html/#reference): Documentation of Spring Kafka
- [Hexagonal Architecture](https://medium.com/ssense-tech/hexagonal-architecture-there-are-always-two-sides-to-every-story-bc0780ed7d9c):
  A blog post on a good way of architecting code. Completely optional, just keep your code clean and organised!
- [Intro to Kafka](https://thenewstack.io/apache-kafka-primer/): A quick read introducing you to Kafka.

# FEEL FREE TO EDIT THIS FILE

## How to run your App?

## Screenshots

### Etc..
